# SO Lab. T4 - Progetto d'esame

Breve descrizione dei file presenti nel repository:

- **manager.c**
> Si occupa della creazione dei singoli studenti e dell'inizializzazione delle principali strutture dati condivise.

- **student.c**
> Si occupa di inizializzare i dati un singolo studente e di applicare la strategia scelta.

- **lib.h**
>  Header file contenente le macro e le strutture dati principali.

- **bool.h**
> Header file contenente la definizione dei valori booleani *TRUE* e *FALSE*.

- **reader.c**
> Si occupa di leggere e formattare il contenuto di **opt.conf**.

- **reader.h**
> Header file contenente il prototipo della funzione principale di **reader.c**

- **sync.c**
> Si occupa di definire le principali funzioni di sincronizzazione.

- **sync.h**
> Header file contenente strutture dati per la sincronizzazione e i prototipi delle funzioni descritte in **sync.c**

- **opt.conf**
> File di configurazione contenente i valori di: *G2*, *G3*, *G4*, *NOF_INVITES*, *MAX_REJECTS*, *POP_SIZE* e *SIM_TIME*.
