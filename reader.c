#include "bool.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

BOOL g2_set = FALSE;
BOOL g3_set = FALSE;
BOOL g4_set = FALSE;
BOOL max_rejects_set = FALSE;
BOOL nof_invites_set = FALSE;
BOOL pop_size_set = FALSE;
BOOL sim_time_set = FALSE;

// Function used to skip consecutives characters equal to "character"
static void skipc(FILE* f, char character) {
    char c = fgetc(f);

    while (c == character) {
        c = fgetc(f);
    }

    // Avoid to exclude the last read character (which is different from "character"'s value)
    fseek(f, ftell(f) - 1, SEEK_SET);
}

// Function used to check if the read string is equal to "correct_str" ("len" is the length of the correct string)
static int check(FILE* f, char* correct_str, size_t len) {
    int r_value = 0;  // "r_value" stands for "return value"
    char read_str[len + 1]; // "len + 1" because we'll add the termination character '\0'
    char value[128];
    BOOL space_found = FALSE;

    // Read the target string
    read_str[0] = correct_str[0];
    for (int i = 1; i < len; i++) {
        read_str[i] = fgetc(f);
    }
    read_str[len] = '\0';

    // If the read string is equal to the correct string, try to find the correct assignment format --> attribute = value
    if (strcmp(correct_str, read_str) == 0) {
        skipc(f, ' ');
        char c = fgetc(f);

        if (c == '=') {
            skipc(f, ' ');
            c = fgetc(f);

            // Read until new line is found
            int i = 0;
            while (c != '\n' && r_value != -1) {

                // If the read character is the ASCII representation of a digit between 0 and 9, concatenate this digit with the previous value
                if (c >= 48 && c <= 57 && !space_found) {
                    value[i] = c;
                    i++;
                }
                else if (c == ' ' || c == '\t') {
                  space_found = TRUE;
                }
                else if (c != ' ' && c != '\t') {
                    r_value = -1;
                }

                c = fgetc(f);
            }
            value[i] = '\0';

            // If the loop hasn't ended because of "r_value", set the return value equal to the integer representation of "value"
            if (r_value != -1) {
                r_value = atoi(value);
            }
        }
        else {
            r_value = -1;
        }
    }
    else {
        r_value = -1;
    }

    return r_value;
}

BOOL scan(char* path, int* g2, int* g3, int* g4, int* max_rejects, int* nof_invites, int* pop_size, int* sim_time) {
    FILE* f = fopen(path, "r");
    char peek;
    BOOL is_valid = TRUE;
    int r;

    while ((peek = fgetc(f)) != EOF && is_valid) {
        // Skip all "blank characters"
        while (peek == ' ' || peek == '\t' || peek == '\n' || peek == '\r') {
            peek = fgetc(f);
        }

        switch (peek) {
          // If read string starts with '#', It represents a comment (skip all characters until '\n')
          case '#':
            while (peek != '\n') {
                peek = fgetc(f);
            }

            break;

          // If read string starts with 'G', It represents the start of a group id
          case 'G':
            peek = fgetc(f);

						if (peek == '2') {
              fseek(f, ftell(f) - 1, SEEK_SET);
							r = check(f, "G2", 2);

							if (r == -1 || (r != -1 && g2_set)) {
	              is_valid = FALSE;
	            }
	            else {
	              *g2 = r;
	              g2_set = TRUE;
	            }
						}
						else if (peek == '3') {
              fseek(f, ftell(f) - 1, SEEK_SET);
							r = check(f, "G3", 2);

							if (r == -1 || (r != -1 && g3_set)) {
	              is_valid = FALSE;
	            }
	            else {
	              *g3 = r;
	              g3_set = TRUE;
	            }
						}
						else if (peek == '4') {
              fseek(f, ftell(f) - 1, SEEK_SET);
							r = check(f, "G4", 2);

							if (r == -1 || (r != -1 && g4_set)) {
	              is_valid = FALSE;
	            }
	            else {
	              *g4 = r;
	              g4_set = TRUE;
	            }
						}
						else {
							is_valid = FALSE;
						}

            break;
          case 'M':
            // If the line starts with 'M', check if the entire string is equal to "POP_SIZE" and if It is well formatted
            r = check(f, "MAX_REJECTS", 11);

            if (r == -1 || (r != -1 && max_rejects_set)) {
              is_valid = FALSE;
            }
            else {
              *max_rejects = r;
              max_rejects_set = TRUE;
            }

            break;
          case 'N':
            // If the line starts with 'N', check if the entire string is equal to "POP_SIZE" and if It is well formatted
            r = check(f, "NOF_INVITES", 11);

            if (r == -1 || (r != -1 && nof_invites_set)) {
              is_valid = FALSE;
            }
            else {
              *nof_invites = r;
              nof_invites_set = TRUE;
            }

            break;

          case 'P':
            // If the line starts with 'P', check if the entire string is equal to "POP_SIZE" and if It is well formatted
            r = check(f, "POP_SIZE", 8);

            if (r == -1 || (r != -1 && pop_size_set)) {
                is_valid = FALSE;
            }
            else {
                *pop_size = r;
                pop_size_set = TRUE;
            }

            break;

          case 'S':
            // If the line starts with 'S', check if the entire string is equal to "SIM_TIME" and it It is well formatted
            r = check(f, "SIM_TIME", 8);

            if (r == -1 || (r != -1 && sim_time_set)) {
                is_valid = FALSE;
            }
            else {
                *sim_time = r;
                sim_time_set = TRUE;
            }

            break;

          default:
            is_valid = FALSE;

            break;
        }
    }

    fclose(f);

    // If the file is well formatted but the sum of all the percentages is not equal to 100, the file is invalid
    int sum = *g2 + *g3 + *g4;
    if (is_valid && (sum != 100 || !sim_time_set || !pop_size_set || !nof_invites_set || !max_rejects_set || !g2_set || !g3_set || !g4_set)) {
        is_valid = FALSE;
    }

    return is_valid;
}
