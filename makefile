all: manager student

manager: manager.o sync.o reader.o
	gcc -Wall manager.o sync.o reader.o -o manager.out -lm
manager.o: manager.c lib.h
	gcc -Wall -c manager.c
sync.o: sync.c lib.h
	gcc -Wall -c sync.c
reader.o:	reader.c
	gcc -Wall -c reader.c
student: student.o sync.o
	gcc -Wall student.o sync.o -o student.out -lm
student.o: student.c lib.h
	gcc -Wall -c student.c
clean:
	rm -f *.o
clean-all:
	rm -f *.o *.out
