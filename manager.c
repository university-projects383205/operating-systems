#include "lib.h"
#include <limits.h>
#include <math.h>
#include <sys/wait.h>
#include "sync.h"
#include "reader.h"

char* ktoa(key_t);
void printShm();
void printStudentInfo(studentInfo * info);

int main(void) {
	int g2, g3, g4, max_rejects, nof_invites, pop_size, sim_time;
	BOOL r = scan("opt.conf", &g2, &g3, &g4, &max_rejects, &nof_invites, &pop_size, &sim_time);

	if (r) {
			printf("[*] Valid file!\n\n");
			printf("G2: %d\n", g2);
			printf("G3: %d\n", g3);
			printf("G4: %d\n", g4);
			printf("MAX_REJECTS: %d\n", max_rejects);
			printf("NOF_INVITES: %d\n", nof_invites);
			printf("POP_SIZE: %d\n", pop_size);
			printf("SIM_TIME: %d\n", sim_time);
	}
	else {
			fprintf(stderr, "[*] Invalid file!\n");
			exit(EXIT_FAILURE);
	}
	//
	// shared table containing even registration number students' informations
	studentInfo even[POP_SIZE];
	// shared table containing odd registration number students' informations
	studentInfo odd[POP_SIZE];

	//---------------------
	// SHARED STUFF INIT...
	//---------------------

	// generating semaphores (SEM)...
	key_t semKey = 123;	// SEM key
	int semId;		// SEM ID
	DO_AND_TEST(semId = semget(semKey, NUM_OF_SEM, IPC_CREAT | IPC_EXCL | 0660));
	unsigned short semVals[NUM_OF_SEM];
	semVals[SHM_MUTEX] = 1;
	semVals[MANAGER_WAIT_0] = POP_SIZE;
	semVals[STUDENT_WAIT_0] = 1;
	union semun arg;
	arg.array = semVals;
	// initializing all semaphore
	/*arg.array[SHM_MUTEX] = 1;
	arg.array[MANAGER_WAIT_0] = POP_SIZE;
	arg.array[STUDENT_WAIT_0] = 1;*/
	DO_AND_TEST(semctl(semId, 0, SETALL, arg));

	// generating shared memory (SM) area...
	key_t shmKey = 12345;	// SM key
	int shmId;		// SM ID
	DO_AND_TEST(shmId = shmget(shmKey, sizeof(shm),
	                           IPC_CREAT | IPC_EXCL | 0660));

	// binding the SM area...
	shm* shmStruct;	// SM address
	DO_AND_TEST(shmStruct = (shm*) shmat(shmId, NULL, 0));

	shmStruct->evenSize = 0;
	shmStruct->oddSize = 0;

	//-------------------
	// SHARED STUFF READY
	//-------------------

	printf("[MANAGER] shm key: %d\n[MANAGER] sem key: %d\n", shmKey, semKey);
	printf("[MANAGER] shm id: %d\n[MANAGER] sem id: %d\n", shmKey, semKey);
	// generating the students...
	int iPop;
	for (iPop = 0; iPop < POP_SIZE; ++iPop) {
		switch (fork()) {	// generating child process
			case -1:	// ERROR CASE
				fprintf(stderr,"Fatal error (#%d): %s",errno,strerror(errno));
				exit(EXIT_FAILURE);
			case 0:		// CHILD CASE
			{
				char* shmKeyStr = ktoa(shmKey);
				printf("shmKeyStr = %s\n", shmKeyStr);
				char* semKeyStr = ktoa(semKey);
				printf("semKeyStr = %s\n", semKeyStr);
				char* args[4] = {"student",shmKeyStr, semKeyStr, NULL};
				// starting the execution of the student code...
				DO_AND_TEST(execve("./student.out", args, NULL));
				free(shmKeyStr);
				free(semKeyStr);
			}
		}
	}
	// waiting until all children have been initialized
	printf("[MANAGER] waiting until all children have been initialized\n");
	wait0(semId, MANAGER_WAIT_0);
	//setAlarm
	sleep(3);
	// authorize child to proceed
	printf("[MANAGER] START\n");
	P(semId, STUDENT_WAIT_0);
	// waiting until all children have ended OR SIM_TIME has passed
	BOOL exitCycle = FALSE;
	while (!exitCycle) {
		wait(NULL);	// waiting for a child to die
		if (errno == ECHILD) exitCycle = TRUE;	// last child is dead
		else if (errno != EINVAL && errno != 0) {	// in any case the manager has recieved an error, exit the code
			fprintf(stderr,"Fatal error (#%d): %s",errno,strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	printf("Printing shared memory:\n");
	printShm(shmStruct);

	// detaching and de-allocating the SM
	shmdt(shmStruct);
	DO_AND_TEST(shmctl(shmId, IPC_RMID, NULL));
	printf("Shared memory removed\n");
	DO_AND_TEST(semctl(semId, 0, IPC_RMID));
	printf("Semaphores removed\n");
	return 0;
}

char* ktoa(key_t key) {
	// maximum characters length of a key
	unsigned int size = floor(log10(key) + 2);
	//unsigned int size = 100;
	printf("size of %d: %d\n", key, size);
	char* buff;	// buffer containing the string representation of the key
	DO_AND_TEST(buff= (char*) calloc(size, sizeof(char)))
	DO_AND_TEST(sprintf(buff, "%d", key));	// converting the key to string...
	return buff;
}

void printStudentInfo(studentInfo* info){
	printf("regNum: %d\n", info->regNum);
	printf("grade: %d\n", info->grade);
	printf("nOfElems: %d\n", info->nOfElems);
	printf("inGroud: %d\n", info->inGroup);
	printf("isClosed: %d\n", info->isClosed);
}

void printShm(shm* shmStruct){
	printf("evenSize: %d\n", shmStruct->evenSize);
	printf("even: \n");
	for(int i = 0; i<shmStruct->evenSize; i++){
		printStudentInfo(&(shmStruct->even[i]));
		printf("\n");
	}
	printf("oddSize: %d\n", shmStruct->oddSize);
	for(int i = 0; i<shmStruct->oddSize; i++){
		printStudentInfo(&(shmStruct->odd[i]));
		printf("\n");
	}
}
