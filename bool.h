#ifndef _BOOL_H_
#define _BOOL_H_

// creating the BOOLEAN type and its possible values
typedef unsigned short BOOL;
#define FALSE 0
#define TRUE !(FALSE)

#endif
