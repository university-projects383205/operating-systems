// binding the library if and only if it hasn't been binded yet
#ifndef _LIB_H_
#define _LIB_H_

#include "bool.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <string.h>

// size of the student population
#define POP_SIZE 5
// maximum simulation time
#define SIM_TIME 20
// number of semaphores
#define NUM_OF_SEM 3
// shared memory mutex
#define SHM_MUTEX 0
// manager semaphore to wait 0
#define MANAGER_WAIT_0 1
// student semaphore to wait 0
#define STUDENT_WAIT_0 2


#define VAL2 20
#define VAL3 50
#define VAL4 30


// macro checking if a FUNC (that can be anything, from an actual
// function call to an arithmetic operation) has generated any error,
// printing said error data and closing the program
#define DO_AND_TEST(FUNC) if ((FUNC) < 0 && errno) {\
	fprintf(stderr,"Fatal error (#%d): %s In file: %s in function: %s at line: %d.\n",errno,strerror(errno), __FILE__,__func__, __LINE__	);\
	exit(EXIT_FAILURE);\
}

// structure representing a student
// the structure contains the following data:
// - regNum | matricola = the registration number of the student in the system
// - grade | voto_AdE = the evaluation obtained by the student at the past exam
// - nOfElems | nof_elems = the prefered group size of the student
// - inGroup = control value: TRUE -> the student is part of a group, FALSE -> otherwise
// - isClosed = control value: TRUE -> the student's group is closed, FALSE -> otherwise
typedef struct student_info {
	unsigned int regNum;		// matricola
	unsigned short grade;		// voto_AdE
	unsigned short nOfElems;	// nof_elems
	BOOL inGroup;
	BOOL isClosed;
} studentInfo;

// structure representing the shared memory data structure
// the structure contains the following data:
// - even = the table containing all the students with even registration number
// - odd = the table containing all the students with off registration number
// - evenSize = number of elements in even array
// - offSize = number of elements in odd array
typedef struct shm{
	studentInfo even[POP_SIZE];
	studentInfo odd[POP_SIZE];
	unsigned int evenSize;
	unsigned int oddSize;
} shm;


#endif
