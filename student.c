#include "lib.h"
#include "sync.h"
// possible message types definition
#define ANSWER 0
#define INVITE 1

// structure representing a message content
// the structure contains the following data:
// - type = either ANSWER or INVITE
// - regNum = regNum of the sender
// - data = either the grade of the sender or the answer
//			to an invite (in said case TRUE or FALSE)
typedef struct message_data {
	short type;
	unsigned int regNum;
	unsigned short data;
} message;

// structure representing the actual message
// the structure contains the following data:
// - mtype = the PID of the reciever
// - mtext = the content of the message (see the previous struct)
typedef struct msgbuf {
	long mtype;
	message mtext;
} msgbuf;

short getPref();

int main(int argc, char ** argv) {
	for(int i = 0; i < argc; i++){
		printf("argv[%d] : %s\n", i, argv[i]);
	}
	printf("[%d] Starting execution...\n", getpid());
	key_t shmKey = (key_t)atoi(argv[1]);
	key_t semKey = (key_t)atoi(argv[2]);
	//key_t msgKey = (key_t)atoi(argv[3]);
	//key_t msgKey = atoi(argv[1]);

	//printf("[%d] shm key: %d\n[%d] sem key: %d\n [%d] msg key: %d", getpid(), shmKey, getpid(), semKey, getpid(), msgkey);

	//printf("[%d] 0\n", getpid());

	//Get the IPC facilities
	int shmId;
	int semId;
	//int msgId;
	DO_AND_TEST(shmId = shmget(shmKey, 0 , 0660));
	DO_AND_TEST(semId = semget(semKey, 1, 0660));
	//DO_AND_TEST(msgId = msgget(msgKey, 0660));

	//printf("[%d] 1\n", getpid());

	shm* shmStruct;
	DO_AND_TEST(shmStruct = (shm*) shmat(shmId, NULL, 0));

	//printf("[%d] 2\n", getpid());

	srand(getpid());

	// initialize data
	studentInfo myData;
	myData.grade = 18 + rand() % 13;
	myData.nOfElems = getPref();
	myData.regNum = getpid();
	myData.inGroup = FALSE;
	myData.isClosed = FALSE;

	printf("[%d] Starting to write...\n", myData.regNum);

	// write datas on shm
	DO_AND_TEST(P(semId, SHM_MUTEX));
	if (myData.regNum % 2 == 0) {
		shmStruct->even[shmStruct->evenSize] = myData;
		++(shmStruct->evenSize);
	} else {
		shmStruct->odd[shmStruct->oddSize] = myData;
		++(shmStruct->oddSize);
	}
	DO_AND_TEST(V(semId, SHM_MUTEX));
	// decrement MANAGER_WAIT_0 by 1
	printf("[%d] decrement MANAGER_WAIT_0 by 1\n", getpid());
	P(semId, MANAGER_WAIT_0);
	// waiting for manager authorization to proceed
	printf("[%d] waiting for start\n", getpid());
	wait0(semId, STUDENT_WAIT_0);


//LA PARTE SOTTOSTANTE PARTE E' SOLO UN PROTOTIPO
#if 0
	msgbuf receivedMessage; //struct containing the receivedMessage
	message answer[POP_SIZE];
	message invite[POP_SIZE];
	int inviteSize = 0, answerSize = 0;
	int pendingInvites = 0;
	while(1){
		//waiting to enter in the critical section
		P(semId, MSG_MUTEX);
		//read all the message received and filter them
		while(msgrcv(msgId, &receivedMessage, sizeof(message), myData.regNum,  IPC_NOWAIT)!=-1){
			if(receivedMessage.mtext.type == ANSWER){
				answer[answerSize] = receivedMessage.mtext;
				answerSize++;
			}
			else{
				invite[inviteSize] = receivedMessage.mtext;
				inviteSize++;
			}
		}

		// analyze all the answers
		int i = 0;
		for(;i<answerSize; i++){
			if(answer[i].data<18){
				// is a rejection
				pendingInvites--;
			}
			else{
				pendingInvites--;
				/*
				if(non sono gia capogruppo){
					divento capogruppo;
					aggiungo le mie informazioni nelle informazioni del gruppo;
					imposto la dimensione del gruppo a 1;
					aggiungi il grupppo alla lista di gruppi;

				}
				aggiungo le informazioni della persona invitata nelle infomrazioni del gruppo;
				aumento la dimensione del gruppo;
				se la dimensione e uguale a quella desiderata chiudo il gruppo;
				*/
			}
		}

		// analyze all the invites
		for(i = 0; i<inviteSize; i++){
			/*
			if(pendingInvites>0){
				rifiuto tutti gli inviti;
			}
			else if(sono capogruppo){
				rifiuto tutti gli inviti;
			}
			else if(ho esaurito i rifiuti){
				o accetto il primo il invito che leggo e rifiuto i rimanenti
				oppure scelgo di rimanere da solo
			}
			else{
				applico la strategia
			}
			*/
		}

		// invite oher student
		 /*
		 if(!ho accettato inviti && sonoCapogruppo){
		 	invito seguendo la strategia e le regole;
		 }
		 else{
		 	non posso invitare;
	 	 }
		 */
	 }
	 V(semId, MSG_MUTEX);
	}
#endif


}

short getPref() {
	// generating random value
	int val = rand() % 100;
	// selection based on probabilities
	if (val < VAL2) {
		return 2;
	}
	else if (val < VAL2 + VAL3) {
		return 3;
	}
	else {
		return 4;
	}
}
